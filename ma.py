# -*- coding: utf-8 -*-
""" CLI tool for mv"""

import argparse
from requests import get
from sys import exit
from time import time, sleep

parser = argparse.ArgumentParser(description="Command line tools for calculating moving average")
parser.add_argument("currenyPair", type=str, help="The currency pair to track. Example: BTC_ETH, BTC_NXT")
parser.add_argument("--window", "-w", type=int, default=60, help="The length in second of the moving average")
args = parser.parse_args()

# default
ma = 0
url = "https://poloniex.com/public?command=returnTradeHistory&currencyPair={currenyPair}&start={startT}&end={endT}"

# args
currenyPair = args.currenyPair
window = args.window

def getTradeHistory(currenyPair, startTime, endTime):
  """Get the trade history from poloniex server

      :param currencyPair: currency pair to track
      :param startTime: when to start tracking
      :param endTime: when to end tracking

      :rtype: list
  """
  r = get(url.format(currenyPair=currenyPair, startT=startTime, endT=endTime))
  if r.status_code != 200: printError("request failed to connect to poloniex server")
  resp = r.json()
  if "error" in resp: printError("request failed: [{err_msg}]".format(err_msg=resp["error"]))
  return r.json()

def calculateMovingAverage(currenyPair, window):
  """Calculate a simple moving average of the provided currency pair and window

    :param currencyPair: currency pair to track
    :param window: the length of time in second to track

    :rtype: float
  """
  global ma
  now = int(time())
  if window < 1: printError("window must be greater than 1")
  trades = getTradeHistory(currenyPair, now - window, now)
  if len(trades) > 0: ma = sum((float(trade["rate"]) for trade in trades)) / len(trades)
  return ma

def printError(msg):
  """Print error msg and exit

    :param msg: the error message to print before exiting
  """
  print(msg)
  exit(1)

try:
  while True:
    print(calculateMovingAverage(currenyPair, window))
    sleep(window)
except KeyboardInterrupt: exit(0)
