### Simple Python tool to calculate moving average of crypto currency pair

python version:
- >=2.7.11
- >=3.3.0

#### Installation
```sh
git clone git@bitbucket.org:jasonator/crpyto_ma.git
cd crpyto_ma
pip install -r requirements.txt
```

#### Run
```python
python </path/to/repo>/ma.py <currency_pair> [-w window_length_in_second]
```
